import React, { FormEvent, useState } from 'react';

import Input from '../../components/Input';

import { useHistory } from 'react-router-dom';

import './styles.css';

const Login = () => {

    const [login, setLogin] = useState('');
    const [pass, setPass] = useState('');
    const [title, setTitle] = useState('Seja bem-vindo!');
    const [showChoose, setShowChoose] = useState(true);
    const [showLogin, setShowLogin] = useState(false);

    const history = useHistory();

    async function handleLogin(e: FormEvent){
        e.preventDefault();
        history.push('/users');
    }

    function handleShowLogin(){
        setShowLogin(true);
        setShowChoose(false);
        setTitle('Login de Usuário');
    }

    return (
        <div className="limiter">
            <div className="container-login">
                <div className="box-login">

                    <div className="box-title">
                        <p className="text-title">{title}</p>
                    </div>

                    { showChoose ? 
                    <div className="box-choose">
                        <button 
                            type="submit" 
                            className="btn-choose"
                            onClick = {() => history.push('/subscribe')}
                        >Cliente</button>
                        <button 
                            type = "submit" 
                            className = "btn-choose" 
                            onClick = {handleShowLogin}
                        >Atendente
                        </button>
                    </div> : null }

                    { showLogin ? 
                    <div className="box-body">
                        <form onSubmit={handleLogin}>
                                            
                            <Input 
                                name="Nome" 
                                label="nome"
                                value = {login}
                                onChange={(e) => setLogin(e.target.value)}
                                icon="usericon"
                                placeholder="Login"
                            />

                            <Input 
                                name="CPF" 
                                label="nome"
                                type="password"
                                value = {pass}
                                onChange={(e) => setPass(e.target.value)}
                                icon="userbordericon"
                                placeholder="Senha"
                            />

                            <button 
                                type="submit" 
                                className="btn-login"
                                onClick = {handleLogin}
                            >Login
                            </button>

                        </form>

                    </div> : null}
                        
                </div>                
            </div>            
        </div>       
    );
}

export default Login;