import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Login from "./pages/Login";
import Subscribe from "./pages/Subscribe";
import Users from './pages/Users';

// rotas para as páginas criadas
function Routes() {
    return (
        <BrowserRouter>
            <Route path="/" exact component={Login} />
            <Route path="/subscribe" component={Subscribe} />
            <Route path="/users" component={Users} />
        </BrowserRouter>        
    );
}
  
export default Routes;