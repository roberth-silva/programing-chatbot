const express = require('express');

const ConversationController = require('./controller/ConversationController');

const routes = express.Router();

routes.get('/conversations', ConversationController.index);
routes.post('/conversation', ConversationController.store);
routes.delete('/conversation', ConversationController.destroy);

module.exports = routes;