const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  login: String,
  pass: String,
  type: String,
  status: String
});

const model = mongoose.model("user", schema);

module.exports = model;