# Desafio para o Módulo Chatbot
Projeto de implementação/manutenção do módulo chatbot

<div align='center'>
<a href='#apresentacao'>Apresentação </a>|
<a href='#requisitos'>Alterações Requisitos atendidos </a>|
<a href='#tecnologias'>Tecnologias </a>|
<a href='#instalacao'>Instalação </a>
</div>

## <p id='apresentacao'>Apresentação</p>
Projeto de implementação/manutenção do módulo chatbot
---

## <p id='requisitos'>Alterações e Requisitos Atendidos</p>
AÇÕES NO BACK-END

npm install - instalação de dependencias/inclusao da pasta node modules

- No arquivo Index.js, instrução errada: const { model } = require('./services/conversations');
  - Correto: const { model } = require('./services/conversation');

- No arquivo conversation.js, instrução errada: const { Schema } = require("monoose");
  - Instrução Correta: const Schema = mongoose.Schema;

- No arquivo conversation.js, instrução errada:
	/**Instrução Errada 
	const schema = new Schema({
	  name,
	  cpf,
	  email,
	});
	*/
  Instrução correta:
	//Instrução Correta
	const schema = new Schema({
	  name: String,
	  cpf: String,
	  email: String,
	});

Instrução errada: module.exports = { model };
Instrução correta: module.exports = model;


não tem conexão com a base de dados
- criado o diretorio database
  - criado o arquivo connection.js


FRONT END

No arquivo services/sessios.js:
 - a url base da api não está especificada da forma correta/adequada. Porta diferente do backend, método getData inexistente
	- forma inadequada:
		export const fetch = axios.get("http://localhost:3005/getData");

	- forma mais adequada:
		const api = axios.create({
		    baseURL: "http://localhost:3001"
		});

		export default api;

Criando todo o projeto frontend
 - adicionado o diretório assets, para imagens, css globais e demais midias
 - adicionado o diretorio components, para componentes de reutilização react
 - adicionado o diretório pages, para as páginas em si de interação do usuário
 - adicionado arquivo services/api.js
 - instalando o babel para usar arquivos .jsx

 Os objetivos principais do desafio foram atendidos
 Os objeticos opcionais, não foram implementados

---


## <p id='tecnologias'>💻 Tecnologias </p>

<a href='https://pt-br.reactjs.org/'>React</a>
<br/>
<a href='https://www.typescriptlang.org/'>TypeScript</a>
<br/>
<a href='https://nodejs.org/en/download/'>Node.js</a>
<br/>

## <p id='instalacao'> ⚙ Instalação e Start </p>
Para instalar e startar a aplicação em seu ambiente, siga os passos abaixo:

### Instalação

Clone o repositório:
```
https://gitlab.com/roberth-silva/programming-challenge.git
```

### 📦 Executar Backend

```bash
# Entre na pasta do servidor
$ cd programming-challenge/back-end

# Instalar as dependencias
$ yarn install
ou
$ npm install

# Executar a aplicação
$ yarn start
ou
$ npm run start
```
Accesso a API pelo endereço http://localhost:3001

### 💻 Executar o projeto frontend

```bash
# Entre na pastad a aplicação web
$ cd programming-challenge/front-end

# Instale as dependências
$ yarn install
ou
$ npm install

# Execute a aplicação
$ yarn start
ou
$ npm start
```
A aplicação executará no seguinte endereço http://localhost:3000/ ou em outra porta que o framework escolher.


Desenvolvido por [Roberth Silva](https://gitlab.com/roberth-silva) 🚀.